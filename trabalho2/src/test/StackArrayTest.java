package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import domain.StackArray;

public class StackArrayTest {
	
	
	@Test
	//Capacidade default deve ser 10
	public void testStackDefaultCapacity() {
		
		StackArray stack = new StackArray() ;
		
		int result =  stack.size();
		assertEquals(10,result);	
	}
	
	@Test
	//Verifica se o tamanho da capacidade setada esta correta
	public void testStackSetedCapacity() {
		
		StackArray stack = new StackArray(5) ;
		
		int result =  stack.size();
		assertEquals(5,result);	
	}
	
	@Test
	//numeros devem ser inseridos na pilha na sequencia correta
	public void testStackPushSequence() {
		
		StackArray stack = new StackArray(3);
		stack.pop();
		stack.pop();
		stack.pop();
		stack.push(1);
		stack.push(2);	
		stack.push(3);	
		int result =stack.peek();
		assertEquals(3,result);	
	}
	
	@Test
	//  numeros nao devem ser inseridos quando a pilha tiver cheia
	public void testStackPushLimit() {

		StackArray stack = new StackArray(3) ;
		stack.pop();
		stack.pop();
		stack.pop();
		stack.push(1);
		stack.push(2);	
		stack.push(3);	
		stack.push(4);	
		int result = stack.peek();
		assertEquals(3,result);	
	}
	
	@Test
	// retirar um item da pilha vazia deve retornar -1
	public void testEmptyStackPop() {

		StackArray stack = new StackArray(1) ;
		stack.pop();
		int result = stack.pop();
		assertEquals(-1,result);	
	}
	
	
	@Test
	// pegar item no topo da pilha 
	public void testStackPeeK() {

		StackArray stack = new StackArray() ; 
		stack.pop(); //eu tiro um item para colocar outro
		stack.push(1);
		int result = stack.peek();
		System.out.println(result);
		assertEquals(1,result);	
	}
	
	@Test
	// pegar item no topo da pilha depois de retirar item da pilha 
	public void testStackPeekAfterPop() {

		StackArray stack = new StackArray(3) ;
		stack.pop();
		stack.pop();
		stack.pop();
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.pop();
		int result = stack.peek();
		assertEquals(2,result);	
	}
	
	@Test
	// remover mais itens do que existe na pilha
	public void testStackPopAllPush() {
		
		StackArray stack = new StackArray(2);
		
		stack.pop();
		stack.pop();
		stack.pop();
		int result = stack.peek();
		
		assertEquals(-1,result);	
	}
	
	
	@Test
	// pegar item no topo da pilha vazia 
	public void testEmptyStackPeeK() {

		StackArray stack = new StackArray(0);
		int result = stack.peek();
		assertEquals(-1,result);	
	}
	
	@Test
	// retirar um item da pilha , deve retornar o item retirado
	public void testStackPop() {

		StackArray stack = new StackArray() ;  
		stack.pop();  
		stack.push(3);
		
		int result = stack.pop();
		assertEquals(3,result);	
	}
	

	@Test
	// verifica se a pilha esta vazia
	public void testEmpyStack() {

		StackArray stack = new StackArray(2) ;
		stack.pop();
		stack.pop();
		
		boolean result = stack.isEmpty();
		assertTrue(result);
	}
	
	@Test
	// verifica se a pilha n�o esta vazia
	public void testNotEmpyStack() {

		StackArray stack = new StackArray() ;// pilha j� � criada com tamanho default 10 e com valores 0 setados

		boolean result = stack.isEmpty();
		assertFalse(result);
	}
	
	
	@Test
	// verifica se a pilha esta cheia
	public void testStackPushIsFull() {

		StackArray stack = new StackArray() ;// pilha j� � criada com tamanho default 10 e com valores 0 setados

		boolean result = stack.isFull();
		assertTrue(result);	
	}
	

	@Test
	// verifica se os itens s�o sobrepostos ap�s a chamada do makeempty
	public void testStackMakeEmpty() {

		StackArray stack = new StackArray() ;
		stack.push(1);
		stack.push(2);
		stack.makeEmpty();
		stack.push(3);
		stack.push(4);
		int result = stack.peek();
		assertEquals(4,result);	
	}
	
	

}