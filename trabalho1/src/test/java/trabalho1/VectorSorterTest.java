package trabalho1;



import static org.junit.Assert.assertArrayEquals;

import org.junit.jupiter.api.Test;

import br.unicamp.ic.inf300.VectorSorter;

public class VectorSorterTest {

	@Test
	//Ordena��o
	public void testSorter() {
		int[] vetor  = {15,6,12,9,3};
		int[] vetorOrdenado = {3,6,9,12,15};
		VectorSorter sorter = new VectorSorter(vetor) ;
		sorter.sort();
		int[] result = sorter.getSorter();
		assertArrayEquals(vetorOrdenado,result);	
	}
}
