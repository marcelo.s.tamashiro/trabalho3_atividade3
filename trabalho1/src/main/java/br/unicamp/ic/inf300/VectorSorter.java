package br.unicamp.ic.inf300;

import org.apache.log4j.Logger;
import org.apache.log4j.BasicConfigurator;

import br.unicamp.ic.inf300.sort.BubbleSort;
import br.unicamp.ic.inf300.sort.Sorter;

/**
 * Classe que para ordenar vetor
 * @author rebec
 *
 */
public class VectorSorter {

	/** Atributo vetor */
	private int[] vector;
	/** Atributo para parametros */
	private static String[] parameters;
	private static final Logger logger = Logger.getLogger(VectorSorter.class);

	public static void main(String[] args) {
		
		BasicConfigurator.configure();
		parameters = args;
		int[] numbers = parseParameters();

		VectorSorter sorter = new VectorSorter(numbers);
		//System.out.print("Input: ");
		logger.info("Input: " + sorter.print());
		//sorter.print();
		sorter.sort();
//		System.out.print("Sorted: ");
		logger.info("Sorted: " + sorter.print());
		//sorter.print();
		
	}

/**
 * 	/** Contrutor que gera numeros randomicos
 * @param size
 *             Tamanho do vetor
 */
	public VectorSorter(int size) {
		vector = generateRandomVector(size);
	}

	/**
	 * /** Construtor que configura o vetor de inteiros
	 * @param numbers
	 *                  N�meros informado
	 */
	public VectorSorter(int[] numbers) {
		vector = numbers;
	}

	/** M�todo para ordenar o vetor*/
	public void sort() {
		Sorter s = new BubbleSort();
		s.sort(vector);
	}

	/** M�todo para recuperar  n�meros inteiros
	 * 
	 * @return Vetor de inteiros*/
	public int[] getSorter(){
		return this.vector;
			
	}
	public static int[] parseParameters() {
		int[] numbers;
		if(parameters.length > 0) {
			numbers = new int[parameters.length];
			for(int k=0; k<parameters.length; k++) {
				numbers[k] = Integer.parseInt(parameters[k]);
			}
		}else {
			numbers = generateRandomVector(10);
		}
		return numbers;
	}

	private static int[] generateRandomVector(int size) {

		int[] vector = new int[size];

		for (int i = 0; i < vector.length ; i++) {
			vector[i] = (int) (Math.random()*100 + 1);
		}

		return vector;
	}
	


//	public void print() {
//		System.out.print("[ ");
//		System.out.print(vector[0]);
//
//		int i = 0;
//		do {
//			i++;
//			System.out.print(", ");
//			System.out.print(vector[i]);
//		}while(i < vector.length - 1);
//
//		System.out.println(" ]");
//	}
	
	public String print() {
		String s = "";
		
		s += "[ ";
		s += vector[0];
		
		int i = 0;
		do {
			i++;			
			s += ", ";			
			s += vector[i];
		}while(i < vector.length - 1);

		s += " ]";		
		return s;
	}
}